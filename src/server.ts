require('dotenv').config()
import express from 'express';
import path from 'path';
import routes from "./routes";

const app = express();
app.use(express.json());
app.use(routes);

app.use(`/${process.env.API_UPLOAD_FOLDER}`, express.static(path.resolve(__dirname, '..', `${process.env.API_UPLOAD_FOLDER}`)))

app.listen(process.env.API_PORT, () => {
    console.log('\x1b[32m%s\x1b[0m', `API escutando na porta: ${process.env.API_PORT}`)
});

