import {Request, Response} from 'express'
import knex from "../database/connection";

class PointsController {
    async store(request: Request, response: Response) {
        const {
            name,
            email,
            whatsapp,
            latitude,
            longitude,
            city,
            uf,
            items,
        } = request.body;
        const trx = await knex.transaction();
        const point = {
            image: 'fake',
            name,
            email,
            whatsapp,
            latitude,
            longitude,
            city,
            uf,
        }
        try {
            const insertedIds = await trx('points').insert(point);
            const point_id = insertedIds[0];
            const pointItems = items.map((item_id: number) => {
                return {
                    item_id,
                    point_id,
                };
            });
            await trx('point_items').insert(pointItems);
            await trx.commit();
            return response.json({
                id: point_id,
                ...point,

            });
        } catch (e) {
            await trx.rollback();
            return response.json({ success: false, message: e });
        }
    }
}

export default PointsController;

